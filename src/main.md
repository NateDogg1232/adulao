# Notes of main.asm and the subroutines it has available

## `load_palette`
- Y: Palette no to load from the palette bank
- X: Palette no to load into (0-3: BG Palettes, 4-7: Sprite palettes)
- A: BG Color to set to. If contains `#$FF` it will keep the original background color
- Rendering is disabled

## `copy_chr_to_ram`
- A pointer to the CHR data in `chr_ram_pointer`
- Rendering is disabled


## Macros that are available

### `infloop`

Self explanitory

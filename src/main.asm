.include "inc/header.inc"
.include "inc/consts.inc"
.include "inc/charmap.inc"
.include "inc/macros.inc"
.include "inc/structs.inc"
.include "inc/zp.inc"
.include "inc/banking.inc"
.include "inc/rodata.inc"


; The main code for A Dream Unlike Any Other

.code

.proc nmi
	; Let's get the controller state
	jsr read_controller

	; And now, let's figure out what state we are in and go from there:
	lda gamestate::title
	cmp zpage::state
	beq do_title

do_title:
	jsr title_screen
	jmp all_done


all_done:
	jsr copy_shadow_oam
	rti
.endproc

.proc title_screen
	; Test the substate of the title screen
	lda titlestate::loadtitle
	cmp zpage::substate
	beq load_title
load_title:

	rts
.endproc

.proc copy_shadow_oam
	lda #$00 ; The address to copy to
	sta OAMADDR
	lda #$02 ; The page to copy from
	sta OAMDMA
.endproc

.proc read_controller
	; At the same time that we strobe bit 0, we initialize the ring counter
	; so we're hitting two birds with one stone here
readjoy:
	lda #$01
	; While the strobe bit is set, buttons will be continuously reloaded.
	; This means that reading from JOYPAD1 will only return the state of the
	; first button: button A.
	sta JOY1
	sta zpage::buttons
	lsr a        ; now A is 0
	; By storing 0 into JOYPAD1, the strobe bit is cleared and the reloading stops.
	; This allows all 8 buttons (newly reloaded) to be read from JOYPAD1.
	sta JOY1
loop:
	lda JOY1
	lsr a	       ; bit 0 -> Carry
	rol zpage::buttons  ; Carry -> bit 0; bit 7 -> Carry
	bcc loop
	rts
.endproc

; tmp_pointer1 contains a pointer to the string
; X contains the X coordinate to put the string
; Y contains the Y coordinate to put the string
.proc print_chars
	txa
	pha
	tya
	pha
.endproc

.proc irq
	rti
.endproc

.proc reset
	; First thing we need to do is ignore IRQs
	sei
	; Then disable decimal mode (Just in case)
	cld
	; Then disable the APU frame IRQ
	ldx #$40
	stx APU + APU::FRAME_CONTROL ; Joy2 is also used to control the frame IRQ
	;And now we set up the stack. We'll keep it at $FF
	ldx #$FF
	txs
	; Clear X
	inx ; X will wrap around from FFh to 0
	stx PPUCTRL ;Disable NMI
	stx PPUMASK ; Disable rendering
	stx APU + APU::DMC_FREQ ; Disable DMC IRQs

	; Now we need to wait for the PPU to stabilize. This takes 2 vblank cycles. We can take the time to do a bunch of stuff we may need
	; Now we clear the vblank flag just in case it's already set beforehand
	bit PPUSTATUS
	; And now we wait for the first vblank
_vblankwait1:
	bit PPUSTATUS ; Check
	bpl _vblankwait1

	; We have a lot of time, so we shall clear the memory with the time we have
	; X is still zero, so no need to reset that, and we can use that to set A
	txa
_memclr:
	sta $000,x
	sta $100,x
	sta $200,x
	sta $300,x
	sta $400,x
	sta $500,x
	sta $600,x
	sta $700,x

	;Increment x
	inx
	; and we branch if x is not zero
	bne _memclr

_vblankwait2:
	; And now we have to wait some more time for the second vblank
	; For now, we'll just let it do its thing
	bit PPUSTATUS
	bpl _vblankwait2

	; And now we set up the PPU
	lda #0
	sta PPUMASK
	bit PPUSTATUS

	lda #$1F ; Set the BG color
	ldx #$00 ; Palette no 0 (BG 0)
	ldy #$00 ; Palette no 0
	jsr load_palette
	lda #$FF ; Keep BG color (A bit faster)
	ldx #$04 ; Palette no 4 (Sprite 0)
	ldy #$00 ; Palette no 0
	jsr load_palette

	ldy #$01
	; Now let's load our spritesheet. We'll switch banks over to the character rom
	jsr bankswitch_y_nosave

	; Then we'll set up our pointer
	lda #<b1chrdata::main_chr
	sta zpage::tmp1
	lda #>b1chrdata::main_chr
	sta zpage::tmp1 + 1

	; And we're off
	jsr copy_chr_to_ram

	; We need to reenable rendering
	lda #PPUMASK::BG_ENABLE | PPUMASK::SPR_ENABLE
	sta PPUMASK
	bit PPUSTATUS ; Gotta read from PPUSTATUS to not fire off the NMI right away
	lda #PPUCTRL::NMI_ENABLE
	sta PPUCTRL

	jmp main
.endproc

.proc main
	infloop
.endproc


; Expects a pointer to the CHR data in ROM in chr_ram_pointer
; All registers are clobbered going into this
; Also, rendering will be turned off
.proc copy_chr_to_ram
	ldy #$0
	sty PPUMASK ; Turn off rendering just to be sure
	sty PPUADDR ; And get our address in which is the beginning of VRAM
	sty PPUADDR
	ldx #$20	; We will be moving 32 pages of 256 bytes each (makes for 8k)
_loop:
	lda (zpage::tmp1),Y
	sta PPUDATA
	iny
	bne _loop ; Z flag will be set to 1 when y wraps around which will be the 256th byte
	inc zpage::tmp1 + 1 ; Let's move onto the next 256 byte page. We do this by incrementing the pointer's MSB
	dex
	bne _loop ; We will loop until X becomes zero
	rts
.endproc

; X should contain the palette number to store the palette into (0-3: BG palettes, 4-7 Sprite palettes)
; Y should contain the palette number to get from the palettes bank
; A should contain the background color you want. $FF means to keep the background color
; All registers are clobbered
; chr_ram_pointer is also written to
; Rendering is disabled as well
.proc load_palette
	pha ; Save A, because we'll be using it a lot
	tya ; Save Y because we'll be using it in a bit
	pha
	; Let's first change our bank to bank 0 without saving it
	ldy #0
	jsr bankswitch_y_nosave
	sty PPUMASK ; Turn off rendering as well
	pla
	tay ; Alright, pull Y back
	; Then, we'll load our palette from our pointer table
	; First start with our pointer
	lda palette::pointer_table,Y
	sta zpage::tmp1
	lda palette::pointer_table+1,Y
	sta zpage::tmp1 + 1
	; Then we'll check if we need to set the background
	txa
	tay ; We'll save X in Y
	pla ; We'll pull out A
	tax
	inx ; If we increment X and it is $FF, it'll wrap around to 0 and set the Z flag
	beq _no_bg
	; So we'll now store A into $3F00 which is where the palette table is
	bit PPUSTATUS
	ldx #$3F
	stx PPUADDR
	ldx #$00
	stx PPUADDR
	sta PPUDATA ; And we'll set that bg color
_no_bg:
	; Then we gotta get our address set up
	bit PPUSTATUS
	lda palette::vram_pointer_table + 1,Y
	sta PPUADDR
	lda palette::vram_pointer_table,Y
	sta PPUADDR
	ldy #$0
	lda (zpage::tmp1),Y
	sta PPUDATA
	iny
	lda (zpage::tmp1),Y
	sta PPUDATA
	iny
	lda (zpage::tmp1),Y
	sta PPUDATA
	; And we should be done, so we'll return the bank to its old one
	jmp bankreturn ;bankreturn already has rts, so we will just use its rts
.endproc



.segment "SHADOW_OAM"
shadow_oam:


.segment "STUB"
reset_stub:
	.addr nmi
	.addr reset
	.addr irq

; vim: filetype=asm_ca65

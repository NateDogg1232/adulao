; A collection of all the structs

.struct oamentry
	y_coord .byte
	index .byte
	attributes .byte
	x_coord .byte
.endstruct

.struct item
	name .res 16
	attack .byte
	magic .byte
	defense .byte
	attributes .byte
.endstruct
.enum itemattrib
	type = %00000011
	equipped = %00000100
.endenum
.struct character ; This includes enemies
	name .res 16
	spells .res 16
	sprite_no .byte
	palette_no .byte
	equipped_weapon .byte
	equipped_armor .byte
	equipped_accessory .byte
	max_hp .byte
	max_mp .byte
	attributes .byte
.endstruct
.enum charattrib
	friendly	= %00000001
	in_party	= %00000010
	dead		= %00000100
.endenum


.enum gamestate
	title
.endenum
.enum titlestate
	loadtitle
	waitbtnpress
.endenum


.out "Included structs"


; vim: filetype=asm_ca65

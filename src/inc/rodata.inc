.segment "BANK0"
.scope palette
; This will store a bunch of palettes that we can use. We'll store a pointer table as well
pointer_table:
	.addr palette0
vram_pointer_table:
	.addr $3F01 ; BG Palette 0
	.addr $3F05 ; BG Palette 1
	.addr $3F09 ; BG Palette 2
	.addr $3F0D ; BG Palette 3
	.addr $3F11 ; Sprite Palette 0
	.addr $3F15 ; Sprite Palette 1
	.addr $3F19 ; Sprite Palette 2
	.addr $3F1D ; Sprite Palette 3
palette0:
	.byte $29
	.byte $17
	.byte $09
.endscope

.segment "BANK1"
.scope b1chrdata 
	main_chr: .incbin "assets/letters.chr"
.endscope

.segment "BANK2"
.scope b2nametable
	title_screen:
.endscope

.segment "BANK3"
.byte "ABCDEFGHIJKLMNOPQRSTUVWXYZ?!1234567890",0

; vim: filetype=asm_ca65

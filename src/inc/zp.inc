.segment "ZEROPAGE"
; This will be a list of different variables that are kept in the zeropage
.scope zpage
	current_bank: .res 1
	state: .res 1
	substate: .res 1
	tmp1: .res 2
	tmp2: .res 2
	tmp3: .res 2
	tmp4: .res 2
	buttons: .res 1
.endscope



; vim: filetype=asm_ca65

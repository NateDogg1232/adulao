.segment "NES2_0"
	.byte "NES", $1A ; Identifier
	.byte 16 ; UNROM has 8 16k banks
	.byte 0 ; No CHR ROM
	.byte $21 ; Mapper 2 with vertical mirroring
	.byte $08 ; NES2.0 identifier
	.byte $00 ; No submapper
	.byte $00 ; PRG ROM is not any larger than 4MiB
	.byte $00 ; No PRG RAM
	.byte $07 ; 8k (64 * 2^7) CHR RAM
	.byte $00 ; NTSC NES
	.byte $00 ; No special PPU
	.byte $00 ; No special expansion device
; vim: filetype=asm_ca65

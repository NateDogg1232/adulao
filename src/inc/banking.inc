; This is where all our banking code will go

.segment "RODATA"

banktable:
	.byte $00, $01, $02, $03, $04, $05, $06
	.byte $07, $08, $09, $0A, $0B, $0C, $0D, $0E

.code
; A is clobbered
bankswitch_y:
	sty zpage::current_bank
bankswitch_y_nosave:
	lda banktable, y
	sta banktable, y
	rts
bankreturn:
	ldy zpage::current_bank
	lda banktable, y
	sta banktable, y
	rts

; vim: filetype=asm_ca65

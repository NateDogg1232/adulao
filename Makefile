ASM=ca65
LD=ld65
ASMFLAGS=-g
SRCDIR=src/
SRC=$(shell find -name *.asm)
OBJ=$(SRC:%.asm=%.o)
PROJNAME=adulao
TARGET=$(PROJNAME).nes

LDFLAGS=-Ln $(PROJNAME).lbl
ccred=\033[0;31m
ccyellow=\033[0;33m
ccend=\033[0m



all: $(TARGET) convlbl

debug: convlbl
	rustynes --ppu_debug -d -l $(TARGET)

convlbl: $(TARGET)
	@sed 's/al 00/$$/;s/ ./#/;s/$$/#/' $(PROJNAME).lbl > $(TARGET).F.nl

fceux: convlbl
	fceux-win $(TARGET)

run: $(TARGET)
	rustynes $(TARGET)

$(TARGET): $(OBJ)
	@echo "[$(ccyellow)INFO$(ccend)] Linking $@"
	ld65 $(LDFLAGS) -C $(PROJNAME).cfg $(OBJ) -o $(TARGET)

%.o: %.asm
	@echo "[$(ccyellow)INFO$(ccend)] Assembling $^"
	$(ASM) $(ASMFLAGS) $^ 

.PHONY: clean
clean:
	@echo "[$(ccyellow)INFO$(ccend)] Cleaning up"
	@rm -rf $(OBJ)
	@rm $(TARGET)
	@rm $(PROJNAME).lbl
	@rm $(TARGET).F.nl

.PHONY: distclean
distclean: clean
	@rm $(TARGET).deb

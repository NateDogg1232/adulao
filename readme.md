# A Dream Unlike Any Other

A JRPG where you're having a dream unlike any other. Details inside the game. More of a tech demo than anything.

## Prerequisites to build:

- You need the cc65 toolchain installed and in your path.

## Prerequisites to run:

- I use Linux. I have the windows build of fceux in my path as fceux-win, so maybe that. You can also change the FCEUX variable in the makefile
- I also use rustynes as a debugger.

## Prerequisites to edit character data

- All my character data and art is in the `art` folder. I use the program `JNESG` to edit my character and other data.

## Prerequisites to install prerequisites

- Just wanted to put this here as a joke.

Have fun in my terrible codebase. I am trying to not follow any guide, so it may not be the best code ever, but I hope it's at least readable. I try to comment as much as possible.
